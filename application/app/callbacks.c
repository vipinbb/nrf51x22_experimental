/////////////////////////////////////////////////////////////////////////
// FILE:     main.c
// PURPOSE:  Main application entry and glue for nrf51x22 project.
// AUTHOR:   Vipin Bakshi
// MODIFIED: 20150321
/////////////////////////////////////////////////////////////////////////
//
//   Copyright (c) 2015 Vipin Bakshi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////

#include "nrf51x22_configuration.h"

#if defined(INCLUDE_BLE)
   #include "blehandler.h"
#endif
#if defined(INCLUDE_ADC)
   #include "adc.h"
#endif
#if defined(INCLUDE_RTC)
   #include "rtc.h"
#endif


#if defined(INCLUDE_RTC)
   void callback_rtc_freq_event(void)
   {
      #if defined(INCLUDE_ADC)
         adc_sample_with_internal_ref(2);
         #if defined(INCLUDE_BLE)
            blehandler_send_hr(adc_get_last_result() | 0x4000);
         #endif

         adc_sample_with_internal_ref(3);
         #if defined(INCLUDE_BLE)
            blehandler_send_hr(adc_get_last_result() | 0x8000);
         #endif

         adc_sample_with_internal_ref(5);
         #if defined(INCLUDE_BLE)
            blehandler_send_hr(adc_get_last_result() | 0xC000);
         #endif
      #endif
   }
#endif

