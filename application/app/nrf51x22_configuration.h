/////////////////////////////////////////////////////////////////////////
// FILE:     main.c
// PURPOSE:  Main application entry and glue for nrf51x22 project.
// AUTHOR:   Vipin Bakshi
// MODIFIED: 20150321
/////////////////////////////////////////////////////////////////////////
//
//   Copyright (c) 2015 Vipin Bakshi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////
#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include "nrf.h"

// APPLICATION INCLUDES
#define INCLUDE_BLE
#define INCLUDE_BLEHRS
#define INCLUDE_RTC
#define INCLUDE_ADC

// RTC CONFIGURATIONS
#if defined(INCLUDE_RTC)
   #define RTC_PERIPHERAL     NRF_RTC1
   #define RTC_FREQUENCY      20
#endif

#include "stdbool.h"
typedef bool(*function_ptr)(void);        // TODO: Move this to some types file.


#endif /* CONFIGURATION_H_ */
