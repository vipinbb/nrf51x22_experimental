/////////////////////////////////////////////////////////////////////////
// FILE:     main.c
// PURPOSE:  Main application entry and glue for nrf51x22 project.
// AUTHOR:   Vipin Bakshi
// MODIFIED: 20150321
/////////////////////////////////////////////////////////////////////////
//
//   Copyright (c) 2015 Vipin Bakshi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nrf51x22_configuration.h"
#include "app_error.h"
#if defined(INCLUDE_BLE)
   #include "blehandler.h"
#endif
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_soc.h"
#if defined(INCLUDE_RTC)
   #include "rtc.h"
#endif


static void main_init();
static void main_post_init();
static void main_clock_init();
static void main_gpio_init();

static bool main_process_pending;


int main(void)
{
	// Application initializations.
   __disable_irq();
   main_init();
   __enable_irq();
   main_post_init();

   main_process_pending = false;

   // Begin application.
	while (1)
	{
	   // Run Multitasking Application.
	   if (blehandler_sm())
	      main_process_pending = true;

	   // Sleep and enter low power if no process pending.
	   if (!main_process_pending)
	      sd_app_evt_wait();
	}

	return 0;
}

void main_init(void)
{
   main_gpio_init();
   main_clock_init();
   #if defined(INCLUDE_RTC)
      rtc_init();
   #endif
}

void main_post_init(void)
{
   #if defined(INCLUDE_BLE)
      blehandler_post_init();
   #endif
   #if defined(INCLUDE_RTC)
     rtc_post_init();
   #endif
}

void main_gpio_init(void)
{
   // Pin configurations.
}

void main_clock_init(void)
{
   // Starts the 16MHz XTAL required for the radio
   NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
   NRF_CLOCK->TASKS_HFCLKSTART = 1;
   while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);
   NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
   NRF_CLOCK->XTALFREQ = UINT32_MAX;

   // Start and Configure the LFCLK
   NRF_CLOCK->LFCLKSRC = (CLOCK_LFCLKSRC_SRC_Synth << CLOCK_LFCLKSRC_SRC_Pos);
   NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
   NRF_CLOCK->TASKS_LFCLKSTART = 1;
   while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0);
   NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;

   NRF_POWER->TASKS_LOWPWR = 1;
   NRF_POWER->TASKS_CONSTLAT = 0;
}


void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
}
