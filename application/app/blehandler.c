#include "nrf51x22_configuration.h"

#if defined(INCLUDE_BLE)

#include "app_error.h"
#include "ble.h"
#include "ble_advdata.h"
#include "blehandler.h"
#include "ble_dis.h"
#include "ble_gap.h"
#include "ble_srv_common.h"
#include "nrf_sdm.h"
#if defined(INCLUDE_BLEHRS)
	#include "ble_hrs.h"
#endif


#include <stdlib.h>


#define DEVICE_NAME                     "nrf51x22"                                   /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "CLOVER" 	                                  /**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                40                                           /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      0                                            /**< The advertising timeout in units of seconds. */

#define SECOND_1_25_MS_UNITS            800                                          /**< Definition of 1 second, when 1 unit is 1.25 ms. */
#define SECOND_10_MS_UNITS              100                                          /**< Definition of 1 second, when 1 unit is 10 ms. */
#define MIN_CONN_INTERVAL               (SECOND_1_25_MS_UNITS / 2)                   /**< Minimum acceptable connection interval (0.5 seconds), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               (SECOND_1_25_MS_UNITS)                       /**< Maximum acceptable connection interval (1 second), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                            /**< Slave latency. */
#define CONN_SUP_TIMEOUT                (4 * SECOND_10_MS_UNITS)                     /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */

#define SEC_PARAM_TIMEOUT               30                                           /**< Timeout for Pairing Request or Security Request (in seconds). */
#define SEC_PARAM_BOND                  1                                            /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                            /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                         /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                            /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                            /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                           /**< Maximum encryption key size. */

static ble_gap_adv_params_t             m_adv_params;                                /**< Parameters to be passed to the stack when starting advertising. */
static ble_gap_sec_params_t             m_sec_params;                                /**< Security requirements for this application. */
static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;     /**< Handle of the current connection. */
static uint32_t m_evt_buffer[CEIL_DIV(sizeof(ble_evt_t) + (BLE_L2CAP_MTU_DEF), sizeof(uint32_t))];
static ble_gap_evt_auth_status_t gap_auth_status;
function_ptr blehandler_sm;
static bool ble_event_pending;

static ble_evt_t* blehandler_get_buffer(void);
static uint32_t blehandler_on_ble_evt(ble_evt_t * p_ble_evt);
static void blehandler_gap_params_init(void);
static void blehandler_advertising_init(void);
static void blehandler_services_init(void);
static void blehandler_sec_params_init(void);
static void blehandler_advertising_start(void);
static bool blehandler_sm_handle(void);


#if defined(INCLUDE_BLEHRS)
	static ble_hrs_t ble_hr_persistent;
	static ble_hrs_init_t hr_init;
#endif

void blehandler_post_init(void)
{
   // Enable the stack and get it going.
   sd_softdevice_enable(NRF_CLOCK_LFCLKSRC_SYNTH_250_PPM, NULL);
   sd_nvic_SetPriority(SWI2_IRQn, NRF_APP_PRIORITY_LOW);
   sd_nvic_EnableIRQ(SWI2_IRQn);

   // Nordic GAP Inits.
   blehandler_gap_params_init();
   blehandler_advertising_init();
   blehandler_services_init();
   blehandler_sec_params_init();
   blehandler_advertising_start();

   blehandler_sm = blehandler_sm_handle;
   ble_event_pending = false;
}

void blehandler_send_hr(uint16_t hr)
{
   ble_hrs_heart_rate_measurement_send(&ble_hr_persistent, hr);
}


static ble_evt_t* blehandler_get_buffer(void)
{
   uint16_t evt_len = sizeof(m_evt_buffer);
   uint32_t err_code;

   err_code = sd_ble_evt_get((uint8_t*) m_evt_buffer, &evt_len);
   if (err_code == NRF_ERROR_NOT_FOUND)
   {
      return NULL;
   }
   else if (err_code != NRF_SUCCESS)
   {
      APP_ERROR_HANDLER(err_code);
   }

   return (ble_evt_t*) m_evt_buffer;
}

/*
 * NORDIC APPLICATION CODE
 */

/**@brief Application's Stack BLE event handler.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static uint32_t blehandler_on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t err_code = NRF_SUCCESS;
    ble_gap_enc_info_t* encryption_info;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            blehandler_advertising_start();
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle,
                                                   BLE_GAP_SEC_STATUS_SUCCESS,
                                                   &m_sec_params);
            break;
        case BLE_GAP_EVT_TIMEOUT:
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
        	err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0);
        	break;

        case BLE_GAP_EVT_SEC_INFO_REQUEST:
        	encryption_info = &gap_auth_status.periph_keys.enc_info;
        	if (encryption_info->div == p_ble_evt->evt.gap_evt.params.sec_info_request.div)
        	{
        		err_code = sd_ble_gap_sec_info_reply(m_conn_handle, encryption_info, NULL);
        	}
        	else
        	{
        		err_code = sd_ble_gap_sec_info_reply(m_conn_handle, NULL, NULL);
        	}
        	break;
        case BLE_GAP_EVT_AUTH_STATUS:
                 gap_auth_status = p_ble_evt->evt.gap_evt.params.auth_status;
                 break;
        default:
            // No implementation needed.
            break;
    }


	#if defined(INCLUDE_BLEHRS)
    	ble_hrs_on_ble_evt(&ble_hr_persistent, p_ble_evt);
    #endif

    	return err_code;
}



/**@brief GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void blehandler_gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode, (uint8_t*)DEVICE_NAME, strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_HEART_RATE_SENSOR_HEART_RATE_BELT);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Advertising functionality initialization.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
static void blehandler_advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    uint8_t       flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

    ble_uuid_t adv_uuids[] =
    {
        {BLE_UUID_HEART_RATE_SERVICE,         BLE_UUID_TYPE_BLE},
        {BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}
    };

    // Build and set advertising data
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance      = true;
    advdata.flags.size              = sizeof(flags);
    advdata.flags.p_data            = &flags;
    advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    advdata.uuids_complete.p_uuids  = adv_uuids;

    err_code = ble_advdata_set(&advdata, NULL);
    APP_ERROR_CHECK(err_code);

    // Initialise advertising parameters (used when starting advertising)
    memset(&m_adv_params, 0, sizeof(m_adv_params));

    m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
    m_adv_params.p_peer_addr = NULL;
    m_adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval    = APP_ADV_INTERVAL;
    m_adv_params.timeout     = APP_ADV_TIMEOUT_IN_SECONDS;
}

/**@brief Initialize services that will be used by the application.
 *
 * @details Initialize the Heart Rate and Device Information services.
 */
static void blehandler_services_init(void)
{
    uint32_t       err_code;
    ble_dis_init_t dis_init;

    // Initialize Device Information Service
    memset(&dis_init, 0, sizeof(dis_init));

    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, MANUFACTURER_NAME);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);

	#if defined(INCLUDE_BLEHRS)
		hr_init.evt_handler = NULL;
		hr_init.is_sensor_contact_supported = false;
    	ble_hrs_init(&ble_hr_persistent, &hr_init);
    #endif
}

/**@brief Start advertising.
 */
static void blehandler_advertising_start(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_start(&m_adv_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Initialize security parameters.
 */
static void blehandler_sec_params_init(void)
{
    m_sec_params.timeout      = SEC_PARAM_TIMEOUT;
    m_sec_params.bond         = SEC_PARAM_BOND;
    m_sec_params.mitm         = SEC_PARAM_MITM;
    m_sec_params.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    m_sec_params.oob          = SEC_PARAM_OOB;
    m_sec_params.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    m_sec_params.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
}

static bool blehandler_sm_handle(void)
{
   if (ble_event_pending)
   {
      ble_evt_t* ble_event = blehandler_get_buffer();
      if (ble_event)
         blehandler_on_ble_evt(ble_event);

      ble_event_pending = true;
   }

   return ble_event_pending;
}


void SWI2_IRQHandler(void)
{
   ble_event_pending = true;
}

#endif
