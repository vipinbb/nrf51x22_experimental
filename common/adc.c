/////////////////////////////////////////////////////////////////////////
// FILE:     adc.c
// PURPOSE:  ADC module source file.
// AUTHOR:   Vipin Bakshi
// MODIFIED: 20150321
/////////////////////////////////////////////////////////////////////////
//
//   Copyright (c) 2015 Vipin Bakshi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////
#include "nrf51x22_configuration.h"

#if defined(INCLUDE_ADC)

static uint16_t adc_result;

void adc_sample_with_internal_ref(uint8_t adc_input_pos)
{
   // interrupt ADC
   NRF_ADC->INTENSET = (ADC_INTENSET_END_Disabled << ADC_INTENSET_END_Pos);                  //!< Interrupt enabled.

   // config ADC
   NRF_ADC->CONFIG   = (ADC_CONFIG_EXTREFSEL_None << ADC_CONFIG_EXTREFSEL_Pos) /* Bits 17..16 : ADC external reference pin selection. */
                                       | ((1 << adc_input_pos) << ADC_CONFIG_PSEL_Pos)              /*!< Use analog input 0 as analog input. */
                                       | (ADC_CONFIG_REFSEL_VBG << ADC_CONFIG_REFSEL_Pos)                   /*!< Use internal 1.2V bandgap voltage as reference for conversion. */
                                       | (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) /*!< Analog input specified by PSEL with 1/3 used as input for the conversion. */
                                       | (ADC_CONFIG_RES_10bit << ADC_CONFIG_RES_Pos);                          /*!< 10bit ADC resolution. */

   // enable ADC
   NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;                                                    // Bit 0 : ADC enable.

   // start ADC conversion
   NRF_ADC->TASKS_START = 1;

   // wait for conversion to end
   while (!NRF_ADC->EVENTS_END)
   {}
   NRF_ADC->EVENTS_END  = 0;

   //Save your ADC result
   adc_result = NRF_ADC->RESULT;

   //Use the STOP task to save current.
   NRF_ADC->TASKS_STOP = 1;
}

uint16_t adc_get_last_result(void)
{
   return adc_result;
}
#endif
