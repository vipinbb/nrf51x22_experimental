/////////////////////////////////////////////////////////////////////////
// FILE:     rtc.c
// PURPOSE:  Commmon RTC driver module source file.
// AUTHOR:   Vipin Bakshi
// MODIFIED: 20150321
/////////////////////////////////////////////////////////////////////////
//
//   Copyright (c) 2015 Vipin Bakshi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////
#include "nrf51x22_configuration.h"

#if defined(INCLUDE_RTC)

#include "nrf.h"
#include "nrf_soc.h"
#include "rtc.h"


#define RTC_PRESCALER         ((32768/RTC_FREQUENCY) - 1)   /* f = LFCLK/(prescaler + 1) */


void rtc_init(void)
{
   RTC_PERIPHERAL->PRESCALER     = RTC_PRESCALER;                    // Set pre-scaler to a TICK of RTC_FREQUENCY.

   // Enable TICK event and TICK interrupt:
   RTC_PERIPHERAL->EVTENSET = RTC_EVTENSET_TICK_Msk;
   RTC_PERIPHERAL->INTENSET = RTC_INTENSET_TICK_Msk;
}

void rtc_post_init(void)
{
   if (RTC_PERIPHERAL == NRF_RTC1)
   {
      sd_nvic_SetPriority(RTC1_IRQn, NRF_APP_PRIORITY_LOW);
      sd_nvic_EnableIRQ(RTC1_IRQn);
   }

   RTC_PERIPHERAL->TASKS_START = 1;
}

void RTC1_IRQHandler()
{
    if ((RTC_PERIPHERAL->EVENTS_TICK != 0) &&
        ((RTC_PERIPHERAL->INTENSET & RTC_INTENSET_TICK_Msk) != 0))
    {
       // Clear event.
       RTC_PERIPHERAL->EVENTS_TICK = 0;
       callback_rtc_freq_event();
    }
}

#endif
